//
//  NJProfileViewController.m
//  NJAirbnb
//
//  Created by  Ching-Fan Hsieh on 7/26/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJProfileViewController.h"
#import "UIColor+Airbnb.h"

@interface NJProfileViewController ()

@end

@implementation NJProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor backgroundViewColor];
}

@end
