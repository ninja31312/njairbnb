//
//  AppDelegate.m
//  NJAirbnb
//
//  Created by  Ching-Fan Hsieh on 7/26/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "AppDelegate.h"
#import "AppUtilities.h"
#import "UIColor+Airbnb.h"
#import "NJExploreToursViewController.h"
#import "NJProfileViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window = window;

    NJExploreToursViewController *exploreToursViewController = [[NJExploreToursViewController alloc] init];
    exploreToursViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:NJLocalizedString(@"EXPLORE") image:[UIImage imageNamed:@"explore"] tag:0];

    NJProfileViewController *profileViewController = [[NJProfileViewController alloc] init];
    profileViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:NJLocalizedString(@"PROFILE") image:[UIImage imageNamed:@"user"] tag:0];
    
    UITabBar.appearance.tintColor = [UIColor mainColor];
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = @[exploreToursViewController, profileViewController];
    window.rootViewController = tabBarController;
    [window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
