//
//  NJExploreToursViewController.m
//  NJAirbnb
//
//  Created by  Ching-Fan Hsieh on 7/26/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJExploreToursViewController.h"
#import "UIColor+Airbnb.h"

@interface NJExploreToursViewController ()

@end

@implementation NJExploreToursViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor backgroundViewColor];
}

@end
