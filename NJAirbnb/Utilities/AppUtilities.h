//
//  AppUtilities.h
//  NJAirbnb
//
//  Created by  Ching-Fan Hsieh on 7/26/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#ifndef AppUtilities_h
#define AppUtilities_h

#define NJLocalizedString(key) (NSLocalizedString(key, key))

#endif /* AppUtilities_h */
