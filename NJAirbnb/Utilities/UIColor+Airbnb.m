//
//  UIColor+Airbnb.m
//  NJAirbnb
//
//  Created by  Ching-Fan Hsieh on 7/26/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "UIColor+Airbnb.h"

@implementation UIColor (Airbnb)

+ (UIColor *)mainColor
{
    return [self _coral];
}

+ (UIColor *)backgroundViewColor
{
    return [UIColor whiteColor];
}

#pragma mark - Pure colors

+ (UIColor *)_coral
{
    return [[UIColor alloc] _initWithHexString:@"fc5c50"];
}

#pragma mark - Utils

- (UIColor *)_initWithHexString:(NSString *)hexString
{
    NSUInteger rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    if ( [hexString hasPrefix:@"#"] ) {
        [scanner setScanLocation:1];
    }
    [scanner scanHexInt:(unsigned int *)&rgbValue];
    return [self _initWithHexNumber:rgbValue];
}

- (UIColor *)_initWithHexNumber:(NSUInteger)hexNumber
{
    CGFloat red   = ((hexNumber & 0xff0000) >> 16) / 255.0f;
    CGFloat green = ((hexNumber & 0x00ff00) >>  8) / 255.0f;
    CGFloat blue  = ((hexNumber & 0x0000ff)      ) / 255.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
}

@end
